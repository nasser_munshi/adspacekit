//
//  AdSpace.swift
//  AdSpaceKit
//
//  Created by Nasser on 7/26/17.
//  Copyright © 2017 WideSpace. All rights reserved.
//

import Foundation

public class AdSpace: UIView {
    
    //-----------------------------
    //MARK: - override methods
    //-----------------------------
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required public init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override public func updateConstraints() {
        super.updateConstraints()
    }
    
    open func runAd() {
        print("run add")
    }
    
}
